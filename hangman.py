#!/usr/bin/env python3
import random


# Get the player's name
while True:
    try:
        name = input('Welcome to Python hangman! Please enter your name: ')
        if not name.isalnum():
            raise ValueError
        if name == 'yourname':
            raise NameError
        break
    except ValueError:
        print("That's not a valid name, please try again.")
    except NameError:
        print("Very funny.")
name = name.upper()
print('Welcome,', name)

quit = False

# Play the game
print("Let's play a game, ",name,", you guess a word I thought up", sep='')

while not quit:
    word = random.choice(open('wordlist.txt').read().splitlines()).upper()
    gameOver = False
    mistakes = 0
    max_mistakes = 5
    guessed=[]

    while not gameOver:
        # Check if the game is over
        if mistakes == max_mistakes:
            break
        if set(word).issubset(set(guessed)):
            break


        print("So far, you have made",mistakes,"mistakes")

        # Print the word with placeholders for unguessed letters
        for i in word:
            if i in guessed:
                print(i.upper(), end='')
            else: print(".", end='')
        print("")

        # Get a valid guess
        while True:
            try:
                guess = input('Either guess a word or a letter: ')
                if not guess.isalpha() or not (len(guess) == 1 or len(guess) == len(word)):
                    raise ValueError
                guess = guess.upper()
                break
            except ValueError:
                print("That's not a valid guess, try again")

        # Handle the guess
        if len(guess) == 1:
            if guess in guessed:
                print("You already guessed that letter!")
            else:
                guessed+=guess
                if guess not in word:
                    mistakes += 1
        elif len(guess) == len(word):
            if guess == word:
                gameOver = True
                break
            else:
                mistakes += 1

    if mistakes == max_mistakes:
        print("You lost! The word was",word)
    else:
        print("You won! The word was indeed",word)

    # Decide if player wants to quit
    while True:
        ans = input("Want to play again? Y/n: ")
        if ans == '' or ans in 'yY':
            print("Alright, let's play again,",name)
            break
        if ans in 'nN':
            quit = True
            break

print("Thank you for playing,",name)

